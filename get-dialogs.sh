#!/bin/bash

PROJECT_NAME="vk-chat-bot"
LOGS_LOCATION=${1?"logs"}

SCR=$( echo "import re"; echo "with open('${LOGS_LOCATION}/${PROJECT_NAME}.log') as f:"; echo "    text=''.join(f.readlines())"; echo "print('\n'.join(re.findall(r'([0-9]+(?:-[0-9]+){2} (?:[0-9]+:){3} (?:<--|-->) \[[0-9-]+\]: \'[^\']*?\')', text, re.MULTILINE)))" ); python3 -c "$SCR"
