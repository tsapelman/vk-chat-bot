#/bin/sh

cd /home/tsapelman/project/telegram/vk-chat-bot
. api.vk-key

while true
do
    ./vk-chat-bot.py -c "${COMMUNITY_ID}" -b "${COMMUNITY_API_KEY}" -a "${ADMIN_API_KEY}" -i background.jpg -t pinkband.jpg --simulate 1 >> logs/vk-chat-bot.log 2>&1
    sleep 5
done
