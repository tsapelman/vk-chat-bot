#!/bin/bash

PROJECT_NAME="vk-chat-bot"

SERVER_LOCATION=$1
PORT=$2
LOCAL_LOGS_LOCATION="logs/server-backup/logs"


if [ "${SERVER_LOCATION: -1}" = ":" ]
then
    SERVER_LOGS_LOCATION="${SERVER_LOCATION}${PROJECT_NAME}/logs"
else
    SERVER_LOGS_LOCATION="${SERVER_LOCATION}/${PROJECT_NAME}/logs"
fi

mkdir -p "${LOCAL_LOGS_LOCATION}"


scp -P "${PORT}" "${SERVER_LOGS_LOCATION}/*.log*" "${LOCAL_LOGS_LOCATION}/"
