#!/usr/bin/env python3
# encoding: utf-8


import argparse
import os
import json
import time
from datetime import datetime
import requests
import subprocess
import copy
import re
import random


# The data files of the script
SCRIPT_NAME = 'vk-chat-bot'
DATA_DIR = '%s/.%s' % ( os.path.expanduser("~"), SCRIPT_NAME )
BOT_DATA_FILE = 'bot-data.txt'
BOT_DATA_FILE_PATH = os.path.join(DATA_DIR, BOT_DATA_FILE)
USER_DATA_FILE = 'user-data.txt'
USER_DATA_FILE_PATH = os.path.join(DATA_DIR, USER_DATA_FILE)
DEFAULT_HANDLED_MESSAGE_ID = 0

# The data caches
BOT_DATA_CACHE = False
USER_DATA_CACHE = False
WIKI_CACHE = dict()

# API depended constants
API_REQUEST_URL="https://api.vk.com/method/"
VK_USER_PROFILE_PREFIX = "https://vk.com/id"
VK_COMMUNITY_PREFIX = "https://vk.com/public"
VK_PAGE_PREFIX = "https://vk.com/page"
COMMUNITY_RULES_PAGE_ID = 53856346
API_VERSION = "5.68"
DEFAULT_RANDOM_ID = 20
DEFAULT_GUID = 30
API_ERRORS_TO_SKIP = [ 18, 900, 901, 902 ]

# Various timings
SLEEP_IF_FAIL = 10
PAUSE_AFTER_REQUEST = 0.35
REQUEST_TIMEOUT = 27
HANDLE_ALL_MESSAGES_PERIOD = 12
MEMBERS_POLLING_PERIOD = 18
NAMES_POLLING_PERIOD = 251
ADMINS_POLLING_PERIOD = 599
ALERT_SENDING_PERIOD = 179
AN_HOUR_PERIOD = 60 * 60
HOURS_IN_A_DAY = 24
A_DAY_PERIOD = HOURS_IN_A_DAY * AN_HOUR_PERIOD
TIMEZONE_OF_BOT_LOCATION = 3

# Some request fields
ADMINS_MAX_COUNT = 10
GET_USERS_PER_REQUEST = 800
RATINGS_ON_WIKI_PAGE = 100
PAGINATOR_SIZE = 2
MAX_USER_MESSAGES_TO_HANDLE = 10
MAX_ALERTS_TO_SEND = 10
MAX_CANDIDATES_TO_DISPLAY = 1000

# Command handling consts
CYRILLIC_LETTERS = set([
    'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
    'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
    'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
])
COMMAND_BEGINNING = '/'
IN_COMMAND_SEPARATOR = ':'
IN_FIELD_SEPARATOR = ','
COMMUNITY_AUTHOR_FLAG = '+'

# Text settings
REGULAR_FONT = 'Linux-Biolinum-O-Bold'
CONDENSED_FONT = 'Carlito-Bold'
REGULAR_FONT_EDGE = 12
FONT_POINT_SIZE = 84
SMALL_FONT_POINT_SIZE = 48
TEXT_VERTICAL_OFFSET = 125
TEXT_TWOROW_OFFSET = 60
RATING_TEXT = "рейтинг"
WINNERS_TEXT = "победители"
LINGUISTIC_SEVERAL = [2, 3, 4]
LINGUISTIC_MANY = [0, 5, 6, 7, 8, 9]
IMAGE_TO_POST = 'post-me-to-wall.png'
IMAGE_TO_POST_FILE_PATH = os.path.join(DATA_DIR, IMAGE_TO_POST)
LIMIT_NAMES_TO = 32
ID_HIDDEN_COUNT = 4
ID_DIGITS_TO_SHOW = 2
ID_HIDDEN_CHAR = "x"
USER_SEED_LIMIT = 1000000000

TEXT_ABOUT_RATING = \
    'Посмотрим, кто завоевал лидирующие позиции по итогам завершающейся недели? &#128521;\n' \
    '\n' \
    'P. S. Напоминаем, что каждый понедельник в 09:59 сумма баллов каждого игрока ' \
    'делится на 2 (два), при этом дробная часть, если она есть, не учитывается.'

HELLO_REPLY = \
    'Я, бот Шурик, рад приветствовать Вас в сообществе "Ассоциативная ассоциация"!' \
    ' Актуальное задание можно найти на стене, но прежде чем присылать мне ответ,' \
    ' ознакомьтесь с правилами.\n' \
    '\n' \
    '&#128163; ВНИМАНИЕ! Принимая участие в игре, вы автоматически соглашаетесь' \
    ' с условиями игры и правилами нашего сообщества: %s-%d_%d'


#################
# Bot data fields
#################
BOT_ALERTS = 'alerts'
BOT_FLAG_PAUSED = 'flag_paused'
BOT_GUID = 'guid'
BOT_HANDLED_MESSAGE_ID = 'handled_message_id'
BOT_RANDOM_ID = 'random_id'
BOT_SERIES = 'series'
BOT_SERIES_ARCHIVE = 'archive'
BOT_SERIES_CURRENT = 'current'
BOT_SERIES_EXPECTED = 'expected_event'
BOT_SERIES_FINISHED = 'finished'
BOT_SERIES_REMAINING = 'remaining'
BOT_SERIES_STARTED = 'started'
BOT_SERIES_STATE = 'state'
BOT_SERIES_TASKS = 'tasks'
BOT_SERIES_TASKS_CURRENT = 'current'
BOT_SERIES_TASKS_PREVIOUS = 'previous'
BOT_SERIES_TOTAL = 'total'
BOT_SERIES_TRESHOLD = 'treshold'
BOT_SERIES_WINNERS = 'winners'
BOT_TASKS = 'tasks'
BOT_TASKS_ANSWERS = 'answers'
BOT_TASKS_HINTS = 'hints'
BOT_TASKS_STATE = 'state'
BOT_TASKS_POST_ID = 'post_id'
BOT_TASKS_PHOTO_ID = 'photo_id'
BOT_TASKS_AUTHOR = 'author'
BOT_TASKS_AUTHOR_ID = 'author_id'
BOT_TASKS_AUTHOR_COMMUNITY_FLAG = 'community_flag'


# Series states:
SS_INACTIVE = 0
SS_OPERATIONAL = 1
SS_FINISHED = 2
SS_PRIZED = 3

# Bot task states:
TS_IDLE = 0
TS_PUBLISHED = 1
TS_EXPIRED = 2
TS_DISCLOSED = 3


##################
# User data fields
##################
REF_USER_ID = 0
USER_ADMIN = 'admin'
USER_ALERTS = 'alerts'
USER_ID = 'id'
USER_MEMBER = 'member'
USER_RATING = 'rating'
USER_TASKS = 'tasks'
USER_TASKS_ATTEMPTS = 'attempts'
USER_TASKS_COMPLETED = 'completed'
USER_TASKS_COMPLETION_TIME = 'completion_time'
USER_TASKS_CURRENT = 'current'
USER_TASKS_PREVIOUS = 'previous'
USER_FIRST_NAME = 'first_name'
USER_LAST_NAME = 'last_name'
USER_CHAT_ON = 'chat_on'
USER_SEED = 'seed'


# Event names
ANSWER_DEADLINE_TIME_EVENT = 'AnswersDeadlineTime'
POST_TASK_TIME_EVENT = 'PostTaskTime'
PUBLISH_ANSWER_TIME_EVENT = 'PublishAnswerTime'
PUBLISH_RATING_TIME_EVENT = 'PublishRatingTime'
PUBLISH_CANDIDATES_TIME_EVENT = 'PublishCandidatesTime'
PUBLISH_WINNERS_TIME_EVENT = 'PublishWinnersTime'

# Miscellanious game consts
TASKS_IN_SERIES = 5
ATTEMPTS_TASK_SOLVING = 3
HINTS_COUNT = 3
LINK_TO_COMMUNITY_IM = "http://bot-shurik.info/reply"
RE_HELLO = re.compile(r"привет(ик)?")
WORTH_OF_TASK = 200
WORTH_OF_ATTEMPT = 50
WINNER_ALGORITHM_THRESHOLD = 6
PRIZE_AMOUNT = 3
RATING_PAGE_ID = 53482700
DEFAULT_RATING_THRESHOLD = 1



# Returns current date and time as a string
def dateTimeStr():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')


# Returns current date and time as a seconds since epoch
def dateTimeSinceEpoch(now=False):
    if not now:
        now = datetime.utcnow()
    return int(now.strftime("%s"))


# Returns amount of seconds to the closest future timestamp by the time of the day
def timestampByDayTime(now, h, m, s):
    hour = (h + HOURS_IN_A_DAY - TIMEZONE_OF_BOT_LOCATION) % HOURS_IN_A_DAY
    ts = ( now.replace(hour=hour, minute=m, second=s, microsecond=0) - now ).total_seconds()

    # Did already occure today?
    if ts < 0:
        # Then tommorow
        ts += A_DAY_PERIOD
    logMe("%02d:%02d:%02d will occur in %d seconds" % (h, m, s, ts))
    return ts


# Reads the bot data as json-coded string from the file.
# Returns json data as a dictionary.
def parseBotDataFile():
    try:
        with open(BOT_DATA_FILE_PATH) as f:
            b = json.loads( f.read().replace('\n', '') )
    except (OSError, IOError, IndexError) as e:
        return dict()

    if BOT_ALERTS not in b:
        b[BOT_ALERTS] = list()

    if BOT_FLAG_PAUSED not in b:
        b[BOT_FLAG_PAUSED] = False

    if BOT_GUID not in b:
        b[BOT_GUID] = DEFAULT_GUID

    if BOT_RANDOM_ID not in b:
        b[BOT_RANDOM_ID] = DEFAULT_RANDOM_ID

    if BOT_HANDLED_MESSAGE_ID not in b:
        b[BOT_HANDLED_MESSAGE_ID] = DEFAULT_HANDLED_MESSAGE_ID

    if BOT_RANDOM_ID not in b:
        b[BOT_RANDOM_ID] = DEFAULT_RANDOM_ID

    if BOT_SERIES not in b:
        b[BOT_SERIES] = dict()
        b[BOT_SERIES][BOT_SERIES_ARCHIVE] = list()
        b[BOT_SERIES][BOT_SERIES_CURRENT] = dict()

    sr = b[BOT_SERIES][BOT_SERIES_CURRENT]

    if BOT_SERIES_STATE not in sr:
        sr[BOT_SERIES_STATE] = SS_INACTIVE

    if BOT_SERIES_EXPECTED not in sr:
        sr[BOT_SERIES_EXPECTED] = False

    if BOT_SERIES_TASKS not in sr:
        sr[BOT_SERIES_TASKS] = dict()

    if BOT_SERIES_WINNERS not in sr:
        sr[BOT_SERIES_WINNERS] = list()

    if BOT_TASKS not in b:
        b[BOT_TASKS] = list()

    for t in b[BOT_TASKS]:
        if BOT_TASKS_AUTHOR not in t:
            t[BOT_TASKS_AUTHOR] = dict()
        t[BOT_TASKS_HINTS] = t[BOT_TASKS_HINTS][:HINTS_COUNT]

    logMe("Bot data are parsed as '%s'\n" % str(b))

    return b


# Writes the bot data into the data file as json-coded string
def refreshBotDataFile(bot_data, force=False):
    global BOT_DATA_CACHE

    b = json.dumps(bot_data, sort_keys=True)
    if force or b != BOT_DATA_CACHE:
        try:
            if not os.path.exists(DATA_DIR):
                os.makedirs(DATA_DIR)
            with open(BOT_DATA_FILE_PATH, "w") as f:
                f.write(b)
                BOT_DATA_CACHE = b
        except (OSError, IOError) as e:
            pass


# Reads the users data as json-coded string from the file.
# Returns json data as a dictionary.
def parseUserDataFile():
    try:
        with open(USER_DATA_FILE_PATH) as f:
            user = json.loads( f.read().replace('\n', '') )
    except (OSError, IOError, IndexError) as e:
        return dict()

    if str(REF_USER_ID) not in user:
        user[str(REF_USER_ID)] = dict()
    for k, u in user.items():
        if USER_ADMIN not in u:
            u[USER_ADMIN] = False
        if USER_ALERTS not in u:
            u[USER_ALERTS] = False
        if USER_ID not in u:
            try:
                u[USER_ID] = int(k)
            except ValueError:
                del user[k]
                continue
        if USER_CHAT_ON not in u:
            u[USER_CHAT_ON] = False
        if USER_SEED not in u:
            u[USER_SEED] = 0
        if USER_MEMBER not in u:
            u[USER_MEMBER] = False
        if USER_RATING not in u or u[USER_RATING] < 0:
            u[USER_RATING] = 0
        if USER_TASKS not in u:
            u[USER_TASKS] = dict()
            u[USER_TASKS][USER_TASKS_CURRENT] = False
            u[USER_TASKS][USER_TASKS_PREVIOUS] = False

    logMe("User data are parsed as '%s'\n" % str(user))

    return user


# Writes the users data into the data file as json-coded string
def refreshUserDataFile(user_data, force=False):
    global USER_DATA_CACHE

    user = json.dumps(user_data, sort_keys=True)
    if force or user != USER_DATA_CACHE:
        try:
            if not os.path.exists(DATA_DIR):
                os.makedirs(DATA_DIR)
            with open(USER_DATA_FILE_PATH, "w") as f:
                f.write(user)
                USER_DATA_CACHE = user
        except (OSError, IOError) as e:
            pass


# Mark 'message_id' message in 'chat_id' chat as read
def markMessageAsRead(chat_id, message_id):
    if SIMULATE_TO:
        return True
    reqType = "messages.markAsRead"
    data = dict(peer_id=chat_id, message_ids=message_id)
    reply = sendApiRequest(reqType, COMMUNITY_API_KEY, data)
    if reply and ('response' in reply) and reply['response'] == 1:
        return True
    handleReplyError(reply, reqType)
    return False


# Mark 'chat_id' chat as answered
def markAsAnswered(chat_id):
    if SIMULATE_TO:
        return True
    reqType = "messages.markAsAnsweredDialog"
    reply = sendApiRequest(reqType, COMMUNITY_API_KEY, dict(peer_id=chat_id))
    if reply and ('response' in reply) and reply['response'] == 1:
        return True
    handleReplyError(reply, reqType)
    return False


# Replies to incoming message from 'user' user.
# Returns message_id of the sent message if succeeds,
# returns False otherwise.
def replyToUser(bot_data, user_data, user, text):
    # Returns a string to be sent to the user if the bot
    # can not handle a user message due to game state
    def cannotAnswer():
        return "В настоящее время ответы на задания не принимаются!\n" \
               "Следите за объявлениями на стене сообщества."

    def castUserText(text):
        return text.strip().strip(" .!?'\"").strip().lower().replace('ё', 'е')

    user_id = user[USER_ID]

    # Operate depending on the state the bot currently in
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr
    logSR(bot_data)
    logMe("user_data='%s'\n" % str(user))

    casted_text = castUserText(text)
    if RE_HELLO.fullmatch(casted_text):
        message = HELLO_REPLY % (VK_PAGE_PREFIX, COMMUNITY_ID, COMMUNITY_RULES_PAGE_ID)
    elif sr[BOT_SERIES_STATE] == SS_INACTIVE:
        message = "Сейчас игра неактивна!"

    elif sr[BOT_SERIES_STATE] == SS_OPERATIONAL:
        if sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT] \
                and sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_STATE] == TS_PUBLISHED \
                and not bot_data[BOT_FLAG_PAUSED]:
            if not user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_COMPLETED]:
                publish_rating = False
                summary_in_answer = False
                if casted_text in \
                        sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_ANSWERS]:
                    user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS] -= 1
                    user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_COMPLETED] = True
                    user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_COMPLETION_TIME] = \
                            dateTimeSinceEpoch()
                    extra_attempts_exhausted = \
                            2 - user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS]
                    inc_rating = WORTH_OF_TASK - extra_attempts_exhausted * WORTH_OF_ATTEMPT
                    user[USER_RATING] += inc_rating
                    summary_in_answer = True
                    pos = len( set([
                        u[USER_RATING]
                            for k, u in user_data.items()
                                if u[USER_ID] != REF_USER_ID and u[USER_RATING] >= user[USER_RATING]
                    ]) )
                    message = "Это правильный ответ!\n" \
                              "Поздравляю, Вы заработали %d баллов и переместились " \
                              "на %d-ю позицию в Рейтинге." % (inc_rating, pos)
                    publish_rating = True
                else:
                    user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS] -= 1
                    if user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS] <= 0:
                        user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_COMPLETED] = True
                        summary_in_answer = True
                        message = "Вы ответили неправильно.\n" \
                                   "К сожалению, это была последняя попытка."
                    else:
                        if user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS] == 1:
                            tries = "осталась одна попытка"
                        elif user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS] == 2:
                            tries = "осталось две попытки"
                        else:
                            tries = "осталось %d попыток" \
                                    % user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_ATTEMPTS]
                        message = "Вы ответили неправильно. У вас %s.\nУдачи в игре!" % tries
                if summary_in_answer:
                    rating = user[USER_RATING]
                    if rating // 10 % 10 == 1 or rating % 10 in LINGUISTIC_MANY:
                        points_text = "баллов"
                    elif rating % 10 in LINGUISTIC_SEVERAL:
                        points_text = "балла"
                    else:
                        points_text = "балл"
                    message += "\nИтого набрано %d %s:" % (rating, points_text)
                    message += "\n%s%d_%d" % (
                            VK_PAGE_PREFIX, -COMMUNITY_ID, RATING_PAGE_ID)
                if publish_rating:
                    if USER_FIRST_NAME in user and USER_LAST_NAME in user and (
                            user[USER_FIRST_NAME] or user[USER_LAST_NAME]):
                        pass
                    else:
                        handleRefreshNames(bot_data, user_data)
                    candidates = getCandidates(user_data, DEFAULT_RATING_THRESHOLD)
                    res = postCandidates(bot_data, candidates, post_on_wall=False)
                    if not res:
                        logAlert(
                            bot_data,
                            'Ошибка обновления рейтинга после сообщения пользователя!\n'
                            'Взять на контроль!'
                        )
                        logSR(bot_data)

            elif user[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_COMPLETION_TIME]:
                message = "Ваш правильный ответ уже был принят и учтён."
            else:
                message = "Вы уже исчерпали возможные попытки ответить на текущее задание!"
        else:
            message = cannotAnswer()

    elif sr[BOT_SERIES_STATE] == SS_FINISHED:
        message = cannotAnswer()

    else:   # SS_PRIZED
        message = cannotAnswer()

    logMe("Replying into the chat (id=%d)" % user_id)
    res = sendMessage(user_id, message, bot_data)
    logSR(bot_data)
    logMe("user_data='%s'\n" % str(user))
    return res


# Sends API POST request to VK API, returns server reply
def sendApiRequest(method, token, data):
    if SIMULATE_TO and method not in ["messages.getById", "pages.getTitles"]:
        return dict()
    method = API_REQUEST_URL + method
    data['access_token'] = token
    data['v'] = API_VERSION
    logMe("--> %s: %s" % (method, data))
    try:
        response = requests.post(method, data)
        reply = response.json()
        logMe("<-- '%s'\n" % str(reply))
        return reply
    except IOError as e:
        logMe("Send API request error - '%s'\n" % str(e))
        return False
    except ValueError as e:
        logMe("Parse API response JSON error - '%s'\n" % str(e))
        return False
    finally:
        time.sleep(PAUSE_AFTER_REQUEST)


# Uploads an image from 'image_filename' filename via
# API multipart/form-data POST request to 'req' address.
# Returns server reply
def sendUploadPhotoRequest(req, image_filename):
    if SIMULATE_TO:
        return True
    logMe("Upload --> '%s'" % req)
    try:
        data = { "photo": open(image_filename, "rb") }
        response = requests.post(req, files=data)
        reply = response.json()
        logMe("<-- '%s'\n" % str(reply))
        return reply
    except IOError as e:
        logMe("Send API request error - '%s'\n" % str(e))
        return False
    finally:
        time.sleep(PAUSE_AFTER_REQUEST)


# Handles reply error
def handleReplyError(reply, reqType):
    msg = "Reply to '%s' is not ok!" % reqType
    if reply:
        msg = "%s - %s" % (str(reply), msg)
    logMe("%s\n" % msg)
    time.sleep(SLEEP_IF_FAIL)


# Requests messages for the bot one by one by ids, parses them, refreshes
# the bot and the user data and replies to appropriate user.
# Returns True if succeeds, False otherwise.
def handleIncomingMessages(bot_data, user_data, event_name):
    # Receives a message from API by 'message_id'.
    # Returns a dict(msg='', user_id=0, out=False, eof=False)
    def getMessage(message_id):
        result = dict(msg='', user_id=0, out=False, eof=False)
        if SIMULATE_TO and message_id >= SIMULATE_TO:
            result['eof'] = True
            return result
        reqType = "messages.getById"
        data = dict(preview_length=0, message_ids=message_id)
        reply = sendApiRequest(reqType, COMMUNITY_API_KEY, data)
        if reply and ('response' in reply):
            response = reply['response']
            if response and ('count' in response) and response['count'] == 1 \
                    and ('items' in response):
                items = response['items']
                if not items:
                    # All existing messages are handled
                    result['eof'] = True
                    return result

                if len(items) == 1:
                    item = items[0]
                    if ('id' in item) and item['id'] == message_id:
                        if item['out'] != 0:
                            result['out'] = True
                            return result
                        if 'body' in item:
                            result['msg'] = item['body']
                        if 'user_id' in item:
                            result['user_id'] = item['user_id']
                            return result

        handleReplyError(reply, reqType)
        return result

    # Request API until MAX_USER_MESSAGES_TO_HANDLE or all messages are handled
    messagesHandled = 0
    while messagesHandled < MAX_USER_MESSAGES_TO_HANDLE:
        message_id = bot_data[BOT_HANDLED_MESSAGE_ID] + 1
        result = getMessage(message_id)
        if result['eof']:
            break
        user_id = result['user_id']
        if not result['out']:
            text = result['msg']
            if not text and not user_id:
                return False
            if str(user_id) not in user_data:
                user_data[str(user_id)] = createNewUser(user_data, user_id)
            user = user_data[str(user_id)]
            logIncomingText(user_id, text)

            if not user[USER_CHAT_ON]:
                if user[USER_MEMBER]:
                    if not onSubscribe(user_id, bot_data):
                        return False
                user[USER_CHAT_ON] = True

            sent_message_id = False
            if text:
                # Maybe an admin command?
                sent_message_id, handled = handleAdminCommand(bot_data, user_data, user, text)

                if not handled: # Maybe a user message?
                    sent_message_id = replyToUser(bot_data, user_data, user, text)

                if not sent_message_id:
                    return False

            if not markMessageAsRead(user_id, message_id):
                return False

            refreshUserDataFile(user_data)

            if sent_message_id and sent_message_id == message_id + 1:
                message_id = sent_message_id

        messagesHandled += 1

        bot_data[BOT_HANDLED_MESSAGE_ID] = message_id
        refreshBotDataFile(bot_data)

    return True


# Creates and returns new user record
def createNewUser(user_data, user_id):
    u = copy.deepcopy(user_data[str(REF_USER_ID)])
    u[USER_ID] = user_id
    u[USER_SEED] = random.randint(0, USER_SEED_LIMIT)
    logMe("New user account has just been created as '%s'" % str(u))
    return u


# Refreshes up to ADMINS_MAX_COUNT administrators of the community.
# Updates 'user_data' collection.
# Returns True if succeeds, False otherwise.
def handleRefreshAdmins(bot_data, user_data, event_name):
    if SIMULATE_TO:
        return True
    reqType = "groups.getMembers"
    admins = set()
    data = dict(group_id=COMMUNITY_ID, offset=0, count=ADMINS_MAX_COUNT, filter='managers')
    reply = sendApiRequest(reqType, COMMUNITY_API_KEY, data)
    reply_is_ok = False
    if reply and ('response' in reply):
        response = reply['response']
        if ('items' in response) and ('count' in response):
            reply_is_ok = True
            for item in response['items']:
                if item['role'] in [ 'creator', 'administrator' ]:
                    admins.add(item['id'])

    if not reply_is_ok:
        handleReplyError(reply, reqType)
        return False

    for k, u in user_data.items():
        if k != str(REF_USER_ID):
            status = (u[USER_ID] in admins)
            if (not u[USER_ADMIN] and status) or (u[USER_ADMIN] and not status):
                u[USER_ADMIN] = status
                # Log the event
                logAdminRightsChangedEvent(k, status)

    for a in admins:
        if a != REF_USER_ID and str(a) not in user_data:
            # Someone currently unknown got administrative rights.
            # Create a profile for them
            user = createNewUser(user_data, a)
            user_data[str(a)] = user
            # Log the event
            logAdminRightsChangedEvent(str(a), status)

    return True


# If alerts exist sends them to admins. Updates 'bot_data' collection.
# Returns True if succeeds, False otherwise.
def handleSendAlert(bot_data, user_data, event_name):
    logMe("It is time to send alerts...")
    alerts = bot_data[BOT_ALERTS]
    if not alerts:
        logMe("...but there are no alerts.\n")
        return True

    admins = [
        u for k, u in user_data.items() if u[USER_ID] != REF_USER_ID and u[USER_ADMIN] and u[USER_ALERTS]
    ]
    count = 0
    while alerts and count < MAX_ALERTS_TO_SEND:
        alert = alerts[0]
        for admin in admins:
            ok = sendMessage(
                admin[USER_ID],
                "ALERT: %s!" % alert,
                bot_data,
                update_handled_message_id=True
            )
            if not ok:
                logMe("Can't send ALERT '%s' to user id=%d\n" % (alert, admin[USER_ID]))
                return False
        del alerts[0]
        logMe("ALERT '%s' is sent successfully\n" % alert)
        count += 1

    return True


# Refreshes membership of the known users.
# Update 'user_data' collection.
# Returns True if succeeds, False otherwise
def handleRefreshMembers(bot_data, user_data, event_name):
    if SIMULATE_TO:
        return True
    reqType = "groups.getMembers"
    members = set()
    expected_count = 1
    last_count = -1
    count = 0
    while count < expected_count and count > last_count:
        data = dict(group_id=COMMUNITY_ID, offset=count, count=GET_USERS_PER_REQUEST)
        reply = sendApiRequest(reqType, COMMUNITY_API_KEY, data)
        if reply and ('response' in reply):
            response = reply['response']
            if ('items' in response) and ('count' in response):
                members |= set(response['items'])
                expected_count = response['count']
                last_count, count = count, len(members)
                continue
        handleReplyError(reply, reqType)
        return False

    for k, u in user_data.items():
        if k != REF_USER_ID:
            status = (u[USER_ID] in members)

            # Handle subscription/unsubscription events
            if not u[USER_MEMBER] and status:
                logMe("User %d has just subscribed to the community\n" % u[USER_ID])
                if u[USER_CHAT_ON]:
                    onSubscribe(u[USER_ID], bot_data, update_handled_message_id=True)
            elif u[USER_MEMBER] and not status:
                logMe("User %d has just unsubscribed from the community\n" % u[USER_ID])
                if u[USER_CHAT_ON]:
                    onUnsubscribe(u[USER_ID], bot_data)
            u[USER_MEMBER] = status

    for m in members:
        if m != REF_USER_ID and str(m) not in user_data:
            # Someone subscribes but keeps silence.
            # Unfortunately community can't initiate chat with such a user.
            # That's why the event is not handled by the bot.
            user = createNewUser(user_data, m)
            user[USER_MEMBER] = True
            user_data[str(m)] = user
            logMe("User %d has just subscribed to the community" \
                    " but we are forced to keep silence by VK rules\n" % user[USER_ID])

    return True


# Handle user subscription event. Always succeds.
# Returns True
def onSubscribe(user_id, bot_data, update_handled_message_id=False):
    return True


# Handle user unsubscription event.
# Returns sent message id if succeeds, False otherwise
def onUnsubscribe(user_id, bot_data):
    message = \
            'Вы перестали быть подписчиком сообщества ' \
            '"Ассоциативная ассоциация" &#128546; ' \
            'Мы будем ждать Вашего скорейшего возвращения!'
    return sendMessage(user_id, message, bot_data, update_handled_message_id=True)


# Refreshes the first and last names of the known users.
# Update 'user_data' collection.
# Returns True if succeeds, False otherwise
def handleRefreshNames(bot_data, user_data, event_name=None):
    if SIMULATE_TO:
        return True
    reqType = "users.get"
    ids = [ k for k, v in user_data.items() if v[USER_ID] != REF_USER_ID ]
    count = 0
    while count < len(ids):
        data = dict(user_ids=','.join(ids[count : count+GET_USERS_PER_REQUEST]))
        reply = sendApiRequest(reqType, ADMIN_API_KEY, data)
        if reply and ('response' in reply):
            for u in reply['response']:
                if USER_ID in u and USER_FIRST_NAME in u \
                        and USER_LAST_NAME in u and str(u[USER_ID]) in user_data:
                    ud = user_data[str(u[USER_ID])]
                    ud[USER_FIRST_NAME] = u[USER_FIRST_NAME][:LIMIT_NAMES_TO]
                    ud[USER_LAST_NAME] = u[USER_LAST_NAME][:LIMIT_NAMES_TO]
                else:
                    break
            else:
                count += len(reply['response'])
                continue
        handleReplyError(reply, reqType)
        return False

    return True


# Handles an 'answers deadline' event
def handleAnswersDeadline(bot_data, user_data, event_name):
    sr = checkAndLogEvent(bot_data, event_name)
    if not sr:
        return True

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        pass

    elif sr[BOT_SERIES_STATE] == SS_OPERATIONAL:
        assert sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_SERIES_STATE] == TS_PUBLISHED
        for k, u in user_data.items():
            u[USER_TASKS][USER_TASKS_CURRENT][USER_TASKS_COMPLETED] = True
        res = editExpiringTaskOnTheWall(sr)
        if not res:
            logAlert(
                bot_data,
                'Ошибка редактирования поста истёкшего задания!\n'
                'Взять на контроль!'
            )
        sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_SERIES_STATE] = TS_EXPIRED
        sr[BOT_SERIES_EXPECTED] = POST_TASK_TIME_EVENT

    elif sr[BOT_SERIES_STATE] == SS_FINISHED:
        pass

    else:   # SS_PRIZED
        sr[BOT_SERIES_REMAINING] = TASKS_IN_SERIES
        sr[BOT_SERIES_TOTAL] = TASKS_IN_SERIES
        sr[BOT_SERIES_STARTED] = dateTimeSinceEpoch()
        sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS] = False
        sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT] = False
        sr[BOT_SERIES_WINNERS] = list()
        for k, u in user_data.items():
            u[USER_TASKS][USER_TASKS_PREVIOUS] = False
            u[USER_TASKS][USER_TASKS_CURRENT] = False
            u[USER_RATING] //= 2

        candidates = getCandidates(user_data, DEFAULT_RATING_THRESHOLD)
        res = postCandidates(bot_data, candidates, post_on_wall=False)
        if not res:
            logAlert(
                bot_data,
                'Ошибка обновления рейтинга в момент сброса!\n'
                'Взять на контроль!'
            )

        sr[BOT_SERIES_EXPECTED] = POST_TASK_TIME_EVENT
        sr[BOT_SERIES_STATE] = SS_OPERATIONAL

    logSR(bot_data)
    return True


# Handles a 'post new task' event
def handlePostNewTask(bot_data, user_data, event_name):
    sr = checkAndLogEvent(bot_data, event_name)
    if not sr:
        return True

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        pass

    elif sr[BOT_SERIES_STATE] == SS_OPERATIONAL:
        assert (
            not sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS]
            or sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS][BOT_SERIES_STATE] == TS_DISCLOSED
            ) or (
                sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT]
                and sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_STATE] == TS_IDLE
            )
        if not sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT] \
                or sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_STATE] != TS_IDLE:
            sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS] = \
                    sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT]
            for k, u in user_data.items():
                u[USER_TASKS][USER_TASKS_PREVIOUS] = u[USER_TASKS][USER_TASKS_CURRENT]
                u[USER_SEED] = random.randint(0, USER_SEED_LIMIT)
            if sr[BOT_SERIES_REMAINING] > 0:
                if not bot_data[BOT_TASKS]:
                    logAlert(bot_data, 'Список заданий пуст, игра приостановлена')
                    logSR(bot_data)
                    return False
                sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT] = bot_data[BOT_TASKS][0]
                del bot_data[BOT_TASKS][0]
                sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_STATE] = TS_IDLE
            else:
                sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT] = False
                sr[BOT_SERIES_EXPECTED] = PUBLISH_ANSWER_TIME_EVENT
                logSR(bot_data)
                return True

        if not postTaskOnTheWall(
                bot_data,
                user_data,
                sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_HINTS],
                sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_AUTHOR],
                sr[BOT_SERIES_TOTAL] - sr[BOT_SERIES_REMAINING] + 1):
            logAlert(bot_data, 'Не удалось опубликовать пост с задачей. Жду следующей возможности')
            logSR(bot_data)
            return False
        sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT][BOT_TASKS_STATE] = TS_PUBLISHED
        sr[BOT_SERIES_REMAINING] -= 1
        for k, u in user_data.items():
            u[USER_TASKS][USER_TASKS_CURRENT] = {
                USER_TASKS_ATTEMPTS:        ATTEMPTS_TASK_SOLVING,
                USER_TASKS_COMPLETED:       False,
                USER_TASKS_COMPLETION_TIME: 0
            }
        sr[BOT_SERIES_EXPECTED] = PUBLISH_ANSWER_TIME_EVENT
        if not bot_data[BOT_TASKS]:
            logAlert(bot_data, 'Задания в списке заданий закончились, пополните список')

    elif sr[BOT_SERIES_STATE] == SS_FINISHED:
        pass

    else:   # SS_PRIZED
        pass

    logSR(bot_data)
    return True


# Handles a 'publish answer' event
def handlePublishAnswer(bot_data, user_data, event_name):
    sr = checkAndLogEvent(bot_data, event_name)
    if not sr:
        return True

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        pass

    elif sr[BOT_SERIES_STATE] == SS_OPERATIONAL:
        if sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS]:
            assert sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS][BOT_TASKS_STATE] == TS_EXPIRED
            if not sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT]:
                task_no = sr[BOT_SERIES_TOTAL]
            elif sr[BOT_SERIES_REMAINING] == 0:
                task_no = sr[BOT_SERIES_TOTAL] - 1
            else:
                task_no = sr[BOT_SERIES_TOTAL] - sr[BOT_SERIES_REMAINING] - 1
            if not postAnswerOnTheWall(
                    bot_data,
                    user_data,
                    sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS],
                    task_no):
                logAlert(
                    bot_data,
                    'Не удалось опубликовать ответ на задачу. Жду следующей возможности'
                )
                logSR(bot_data)
                return False
            sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_PREVIOUS][BOT_TASKS_STATE] = TS_DISCLOSED
        if not sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT]:
            assert sr[BOT_SERIES_REMAINING] <= 0
            sr[BOT_SERIES_STATE] = SS_FINISHED
            sr[BOT_SERIES_EXPECTED] = PUBLISH_CANDIDATES_TIME_EVENT
        else:
            sr[BOT_SERIES_EXPECTED] = ANSWER_DEADLINE_TIME_EVENT

    elif sr[BOT_SERIES_STATE] == SS_FINISHED:
        pass

    else:   # SS_PRIZED
        pass

    logSR(bot_data)
    return True


# Returns a list of candidates to be winner.
# Considers a user as a candidate if they reach the given threshold.
def getCandidates(user_data, threshold):
    cc = list()
    for k, u in user_data.items():
        if u[USER_ID] != REF_USER_ID and u[USER_RATING] >= threshold:
            cc.append((
                u[USER_ID],
                u[USER_RATING],
                u[USER_SEED],
                u[USER_FIRST_NAME] if USER_FIRST_NAME in u else "",
                u[USER_LAST_NAME] if USER_LAST_NAME in u else ""
            ))
    cc.sort(key=lambda x: (x[1], x[2]), reverse=True)
    logMe("Candidates: '%s'\n" % str(cc))
    return cc[:MAX_CANDIDATES_TO_DISPLAY]


# Handles a 'publish candidates' event
def handlePublishCandidates(bot_data, user_data, event_name):
    sr = checkAndLogEvent(bot_data, event_name)
    if not sr:
        return True

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        pass

    elif sr[BOT_SERIES_STATE] == SS_OPERATIONAL:
        pass

    elif sr[BOT_SERIES_STATE] == SS_FINISHED:
        handleRefreshNames(bot_data, user_data)
        candidates = getCandidates(user_data, DEFAULT_RATING_THRESHOLD)
        res = postCandidates(bot_data, candidates, post_on_wall=True, text=TEXT_ABOUT_RATING)
        if not res:
            logAlert(bot_data, 'Не удалось опубликовать рейтинг. Жду следующей возможности')
            logSR(bot_data)
            return False

        # Calculate a list of winners
        sr[BOT_SERIES_EXPECTED] = PUBLISH_WINNERS_TIME_EVENT

    else:   # SS_PRIZED
        pass

    logSR(bot_data)
    return True


# Handles a 'publish winners' event
def handlePublishWinners(bot_data, user_data, event_name):
    sr = checkAndLogEvent(bot_data, event_name)
    if not sr:
        return True

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        pass

    elif sr[BOT_SERIES_STATE] == SS_OPERATIONAL:
        pass

    elif sr[BOT_SERIES_STATE] == SS_FINISHED:
        archived_series = dict(
            finished=dateTimeSinceEpoch(),
            started=sr[BOT_SERIES_STARTED],
            total=sr[BOT_SERIES_TOTAL],
            winners=sr[BOT_SERIES_WINNERS]
        )
        bot_data[BOT_SERIES][BOT_SERIES_ARCHIVE].append(archived_series)
        sr[BOT_SERIES_STATE] = SS_PRIZED
        sr[BOT_SERIES_EXPECTED] = PUBLISH_WINNERS_TIME_EVENT

    else:   # SS_PRIZED
        sr[BOT_SERIES_EXPECTED] = ANSWER_DEADLINE_TIME_EVENT

    logSR(bot_data)
    return True


# Sends 'msg' message to the user by 'user_id'. Updates and store bot_data.
# Returns sent message id if succeeds, False otherwise
def sendMessage(user_id, msg, bot_data, update_handled_message_id=False):
    if SIMULATE_TO:
        return 1
    data = dict()
    data['peer_id'] = user_id
    data['random_id'] = bot_data[BOT_RANDOM_ID]
    data['message'] = msg

    logOutgoingText(user_id, msg.strip())
    reqType = "messages.send"
    reply = sendApiRequest(reqType, COMMUNITY_API_KEY, data)

    new_message_id = False
    if reply and ('response' in reply) and reply['response']:
        new_message_id = reply['response']
        if update_handled_message_id and bot_data[BOT_HANDLED_MESSAGE_ID]+1 == new_message_id:
            bot_data[BOT_HANDLED_MESSAGE_ID] = new_message_id
    elif reply and ('error' in reply) and reply['error'] \
            and ('error_code' in reply['error']) \
            and reply['error']['error_code'] in API_ERRORS_TO_SKIP:
        new_message_id = -1
    else:
        handleReplyError(reply, reqType)

    bot_data[BOT_RANDOM_ID] = (bot_data[BOT_RANDOM_ID] + 1) % 2000000000

    return new_message_id


# Checks for correctness of the answers string.
# Returns the list of the answers if correct, False otherwise
def getTaskAnswers(s):
    if not s.startswith('(') or not s.endswith(')'):
        return False

    answers = s[1:-1].split(IN_FIELD_SEPARATOR)
    for i, a in enumerate(answers):
        a = recodeTaskHint(a)
        if not a:
            return False
        answers[i] = a
    return answers


# Checks for correctness of the hint to a task.
# Then recodes the hint by the simple rules.
# Returns recoded hint if the hint is correct, False otherwise
def recodeTaskHint(hint):
    hint_chars = CYRILLIC_LETTERS | set('-')
    hint = hint.strip().lower()

    for ch in hint:
        if ch not in hint_chars:
            return False

    hint = hint.replace('ё', 'е')
    return hint


# Parses strings list as a task description,
# returns appropriate task description dictionary.
# If something went wrong returns False
def taskStringsToDict(task):
    #        answers    hints
    mandatory = 1 + HINTS_COUNT
    #        author
    optional = 1
    if not (mandatory <= len(task) <= mandatory + optional):
        return False, "Недопустимое количество подсказок"

    answers = getTaskAnswers(task[0])
    if not answers:
        return False, "Не удалось разобрать ответы"

    # All answers must be different
    if len(set(answers)) != len(answers):
        return False, "Все ответы должны различаться"

    temp = task[1:]

    hints = list()
    for hint in temp[:HINTS_COUNT]:
        hint = recodeTaskHint(hint)
        if not hint:
            return False, "Недопустимый символ в подсказке"
        hints.append(hint)

    # All hints must be different
    if len(set(hints)) != len(hints):
        return False, "Все подсказки должны различаться"

    temp = temp[HINTS_COUNT:]
    author_str = temp[0] if temp else ''
    community_flag = False
    if author_str.endswith(COMMUNITY_AUTHOR_FLAG):
        community_flag = True
        author_str = author_str[:-1]
    try:
        # Expect VK user id here
        author_id = int(author_str)
    except (IndexError, ValueError):
        author_id = 0
        community_flag = False

    author = dict()
    if author_id:
        author[BOT_TASKS_AUTHOR_ID] = author_id
        author[BOT_TASKS_AUTHOR_COMMUNITY_FLAG] = community_flag

    t = dict()
    t[BOT_TASKS_ANSWERS] = answers
    t[BOT_TASKS_HINTS] = hints
    t[BOT_TASKS_AUTHOR] = author

    return t, ""


# Returns a string generated from a task dictionary
def taskToString(task):
    answers = task[BOT_TASKS_ANSWERS]
    hints = task[BOT_TASKS_HINTS]
    author = task[BOT_TASKS_AUTHOR]

    aulist = [ ]
    if author:
        author_id = author[BOT_TASKS_AUTHOR_ID]
        aulist = [
                str(author_id) + ('+' if author[BOT_TASKS_AUTHOR_COMMUNITY_FLAG] else '')
        ]

    s = IN_FIELD_SEPARATOR.join(answers)
    s = IN_COMMAND_SEPARATOR.join([ '(' + s + ')' ] + hints + aulist)
    return s


# Handlers.
# Get bot data, user data, command fields and user as arguments.
# Where 'user' is the data of the user the request to handle is from.
# Return bot's reply to be sent to the admin


def doHelp(bot_data, user_data, fields, user):
    msg = "Список доступных команд:\n"
    for cmd_desc in ADMIN_COMMANDS:
        msg += "/%s - %s;\n" % (cmd_desc['command'], cmd_desc['desc'])
    return msg[:-2] + '.'


def doList(bot_data, user_data, fields, user):
    if bot_data[BOT_TASKS]:
        msg = "Список заданий:\n"
        for i, task in enumerate(bot_data[BOT_TASKS]):
            msg += "%d. %s\n" % (i+1, taskToString(task))
    else:
        msg = "Список заданий пуст"
    return msg


def doAdd(bot_data, user_data, fields, user):
    START_POS = 1
    task, cause = taskStringsToDict(fields[START_POS:])
    if task:
        bot_data[BOT_TASKS].append(task)
        return "Задание %d добавлено" % len(bot_data[BOT_TASKS])
    return 'Ошибка параметров команды "add": "%s"' % cause


def doInsert(bot_data, user_data, fields, user):
    try:
        pos = int(fields[1])
    except ValueError:
        pass
    else:
        if 1 <= pos <= 1+len(bot_data[BOT_TASKS]):
            START_POS = 2
            task, cause = taskStringsToDict(fields[START_POS:])
            if task:
                bot_data[BOT_TASKS].insert(pos-1, task)
                return "Задание вставлено под номером %d" % pos
    return 'Ошибка параметров команды "insert": "%s"' % cause


def doDelete(bot_data, user_data, fields, user):
    try:
        pos = int(fields[1])
    except ValueError:
        pass
    else:
        if 1 <= pos <= len(bot_data[BOT_TASKS]):
            del bot_data[BOT_TASKS][pos-1]
            return "Задание %d удалено" % pos
    return 'Ошибка параметров команды "delete"'


def doUp(bot_data, user_data, fields, user):
    try:
        pos = int(fields[1])
    except ValueError:
        return 'Ошибка параметров команды "up"'
    if pos == 1:
        return "Команда и так первая списке"

    if 1 < pos <= len(bot_data[BOT_TASKS]):
        bot_data[BOT_TASKS][pos-1], bot_data[BOT_TASKS][pos-2] = \
                bot_data[BOT_TASKS][pos-2], bot_data[BOT_TASKS][pos-1]
        return "Задание %d перемещено вверх" % pos
    return 'Ошибка параметров команды "up"'


def doDown(bot_data, user_data, fields, user):
    try:
        pos = int(fields[1])
    except ValueError:
        return 'Ошибка параметров команды "down"'
    if pos == len(bot_data[BOT_TASKS]):
        return "Команда и так последняя в списке"

    if 1 <= pos < len(bot_data[BOT_TASKS]):
        bot_data[BOT_TASKS][pos-1], bot_data[BOT_TASKS][pos] = \
                bot_data[BOT_TASKS][pos], bot_data[BOT_TASKS][pos-1]
        return "Задание %d перемещено вниз" % pos
    return 'Ошибка параметров команды "down"'


def doMove(bot_data, user_data, fields, user):
    try:
        pos = int(fields[1])
        to = int(fields[2])
    except ValueError:
        return 'Ошибка параметров команды "move"'

    if 1 <= pos <= len(bot_data[BOT_TASKS]) and 1 <= to <= len(bot_data[BOT_TASKS]):
        if pos == to:
            return "Команда и так в заданной позиции"

        task = bot_data[BOT_TASKS][pos-1]
        del bot_data[BOT_TASKS][pos-1]
        bot_data[BOT_TASKS].insert(to-1, task)
        return "Задание %d перемещено в позицию %d" % (pos, to)
    return 'Ошибка параметров команды "move"'


def doStatus(bot_data, user_data, fields, user):
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr

    s = list()
    s.append("paused=" + str(bot_data[BOT_FLAG_PAUSED]))
    s.append("series::current=" + str(sr))
    s.append("tasks=<list of %d>" % len(bot_data[BOT_TASKS]))

    return '\n'.join(s).replace("'", '"')


def doStart(bot_data, user_data, fields, user):
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        sr[BOT_SERIES_EXPECTED] = ANSWER_DEADLINE_TIME_EVENT
        sr[BOT_SERIES_STATE] = SS_PRIZED
        return "Игра запущена."

    else: # SS_OPERATIONAL, SS_FINISHED, SS_PRIZED
        return "Игра и так активна!"


def doPause(bot_data, user_data, fields, user):
    bot_data[BOT_FLAG_PAUSED] = True
    return "Игра приостановлена."


def doResume(bot_data, user_data, fields, user):
    bot_data[BOT_FLAG_PAUSED] = False
    return "Игра возобновлена."


def doReset(bot_data, user_data, fields, user):
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        return "Сейчас игра неактивна!"

    else:   # SS_OPERATIONAL, SS_FINISHED, SS_PRIZED
        sr[BOT_SERIES_EXPECTED] = False
        sr[BOT_SERIES_STATE] = SS_INACTIVE
        return "Игра сброшена."


def doFinalize(bot_data, user_data, fields, user):
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr

    tasks_passed = sr[BOT_SERIES_TOTAL] - sr[BOT_SERIES_REMAINING]
    max_threshold = WORTH_OF_TASK * tasks_passed

    threshold = -1
    if len(fields) == 2:
        try:
            threshold = int(fields[1])
        except ValueError:
            pass

    if not (0 <= threshold <= max_threshold):
        threshold = DEFAULT_RATING_THRESHOLD

    msg = "Игра остановлена."

    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        msg = "Сейчас игра неактивна!"

    elif sr[BOT_SERIES_STATE] in [SS_OPERATIONAL, SS_FINISHED]:
        # Calculate a list of candidates
        handleRefreshNames(bot_data, user_data)
        candidates = getCandidates(user_data, threshold)
        res = postCandidates(bot_data, candidates, post_on_wall=True, text=TEXT_ABOUT_RATING)
        if not res:
            logAlert(bot_data, 'Не удалось опубликовать рейтинг')

        # Calculate a list of winners
        archived_series = dict(
            finished=dateTimeSinceEpoch(),
            started=sr[BOT_SERIES_STARTED],
            total=sr[BOT_SERIES_TOTAL],
            winners=sr[BOT_SERIES_WINNERS]
        )
        bot_data[BOT_SERIES][BOT_SERIES_ARCHIVE].append(archived_series)
        msg = "Игра остановлена с порогом %d." % threshold

    else:   # SS_PRIZED
        pass

    sr[BOT_SERIES_EXPECTED] = False
    sr[BOT_SERIES_STATE] = SS_INACTIVE
    return msg


def doAdvance(bot_data, user_data, fields, user):
    if bot_data[BOT_FLAG_PAUSED]:
        return "Сейчас игра приостановлена!"

    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr
    if sr[BOT_SERIES_STATE] == SS_INACTIVE:
        return "Сейчас игра неактивна!"

    event_name = sr[BOT_SERIES_EXPECTED]
    if not event_name:
        return "Ожидаемое событие сейчас не определено!"

    global events
    for event in events:
        if event['event_name'] and event['event_name'] == event_name:
            ok = event['handler'](bot_data, user_data, event['event_name'])
            if ok:
                return "Игра продвинута."
            else:
                return "При продвижении игры произошла ошибка!"

    return "Не удалось продвинуть игру (внутренняя ошибка)!"


def doAlert(bot_data, user_data, fields, user):
    alert_status = user[USER_ALERTS]

    if fields[1] in ["on", "вкл"]:
        new_status = True
    elif fields[1] == ["off", "выкл"]:
        new_status = False
    else:
        return 'Допустимые значения параметра [on, off] или [вкл, выкл]!'

    str_status = "вкл" if new_status else "выкл"
    if new_status != alert_status:
        user[USER_ALERTS] = new_status
        return "Статус подписки на ALERT изменён на %s." % str_status
    else:
        return "Статус подписки на ALERT (%s) сохранён" % str_status


# Runs given shell command in synchronous mode. Handles errors.
# Returns True if succeeds, False otherwise.
def runShellCommand(cmd):
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = result.stdout.decode('utf-8')
    err = result.stderr.decode('utf-8')
    if out or err:
        return logError(
            "subprocess.run() error!\ncommand: '%s'\nstdout: '%s'\nstderr: '%s'\n"
            % (cmd, out, err)
        )
    else:
        return True


# Executes 'sync' shell command.
# Returns True if succeeds, False otherwise.
def runSync():
    return runShellCommand('sync')


# Posts a text with attached IMAGE_TO_POST_FILE_PATH picture onto the community wall.
# Returns True if succeeded, False otherwise
def postPictureAndTextOnTheWall(bot_data, text=None, attachments=None, store_post_data=None):
    if SIMULATE_TO:
        return True
    if attachments is None:
        attachments = list()

    # Initiate a number of requests to VK API
    msg = '%s request to VK API'

    # 1. photos.getWallUploadServer
    reqType = "photos.getWallUploadServer"
    reply = sendApiRequest(reqType, ADMIN_API_KEY, dict(group_id=COMMUNITY_ID))
    if not reply or ('response' not in reply) or not reply['response']:
        return logError(msg % reqType)
    response = reply['response']
    if not response or ('upload_url' not in response):
        return logError(msg % reqType)
    upload_url = response['upload_url']
    if not upload_url:
        return logError(msg % reqType)

    # 2. Upload photo to the server
    reqType = "upload-photo"
    reply = sendUploadPhotoRequest(upload_url, IMAGE_TO_POST_FILE_PATH)
    if not reply or ('photo' not in reply) or ('server' not in reply) or ('hash' not in reply):
        return logError(msg % reqType)

    # 3. photos.saveWallPhoto
    reqType = "photos.saveWallPhoto"
    data = dict(
        server=reply['server'],
        hash=reply['hash'],
        group_id=COMMUNITY_ID,
        photo=reply['photo']
    )
    reply = sendApiRequest(reqType, ADMIN_API_KEY, data)
    if not reply or ('response' not in reply):
        return logError(msg % reqType)
    response = reply['response']
    if not response or len(response) != 1:
        return logError(msg % reqType)
    response = response[0]
    if not response or ('owner_id' not in response) or ('id' not in response):
        return logError(msg % reqType)
    photo_id = "photo%d_%d" % ( response['owner_id'], response['id'] )

    # 4. wall.post
    reqType = "wall.post"
    attachments.insert(0, photo_id)
    data = dict(
        owner_id=-COMMUNITY_ID,
        from_group=1,
        attachments=','.join(attachments),
        mark_as_ads=0,
        guid=bot_data[BOT_GUID]
    )
    if text:
        data['message'] = text
    bot_data[BOT_GUID] = bot_data[BOT_GUID] + 1

    reply = sendApiRequest(reqType, ADMIN_API_KEY, data)
    if not reply or ('response' not in reply):
        return logError(msg % reqType)
    response = reply['response']
    if not response or ('post_id' not in response):
        return logError(msg % reqType)

    if store_post_data:
        sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
        current_task = sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT]
        current_task[BOT_TASKS_POST_ID] = response['post_id']
        current_task[BOT_TASKS_PHOTO_ID] = photo_id

    return True


# Returns author mention string for further publishing
def getTaskAuthorStr(user_data, author):
    def defaultAuthor():
        return "@club%d (%s)" % ( COMMUNITY_ID, "Ассоциативная ассоциация" )

    if author:
        author_id = author[BOT_TASKS_AUTHOR_ID]
        community_flag = author[BOT_TASKS_AUTHOR_COMMUNITY_FLAG]
        s_author = str(author_id)
        try:
            first_name = user_data[s_author][USER_FIRST_NAME]
            last_name = user_data[s_author][USER_LAST_NAME]
        except KeyError:
            first_name, last_name = "", ""
        addition = ( " и %s" % defaultAuthor() ) if community_flag else ""
        return "Автор - @id%d (%s)%s." % (
                author_id, toHumanReadable(author_id, first_name, last_name), addition )
    else:
        return "Автор - %s." % defaultAuthor()


# Edits a post containing expiring task on the community wall.
# Returns True if succeeded, False otherwise
def editExpiringTaskOnTheWall(sr):
    if SIMULATE_TO:
        return True
    task_no = sr[BOT_SERIES_TOTAL] - sr[BOT_SERIES_REMAINING]
    current_task = sr[BOT_SERIES_TASKS][BOT_SERIES_TASKS_CURRENT]
    post_id = current_task[BOT_TASKS_POST_ID]
    photo_id_str = current_task[BOT_TASKS_PHOTO_ID]
    addition = getTaskAuthorStr(user_data, current_task[BOT_TASKS_AUTHOR])
    text = "Задание №%d. %s\nПриём ответов завершён!" % (task_no, addition)

    msg = '%s request to VK API'
    reqType = "wall.edit"
    data = dict(
        owner_id=-COMMUNITY_ID,
        post_id=post_id,
        attachments=photo_id_str,
        mark_as_ads=0,
        message=text
    )
    return True if sendApiRequest(reqType, ADMIN_API_KEY, data) else logError(msg % reqType)


# Posts a task on the wall. Returns True if succeeded, False otherwise
def postTaskOnTheWall(bot_data, user_data, hints, author, task_no):
    # Produce an image to be posted on the community wall via shell command
    max_len = max([ len(item) for item in hints ])
    font = REGULAR_FONT if max_len < REGULAR_FONT_EDGE else CONDENSED_FONT

    cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d -fill white ' \
            '-annotate +0-%d "%s" -annotate 0 "%s" -annotate +0+%d "%s" %s' % (
            BACKGROUND_IMAGE, font, FONT_POINT_SIZE, TEXT_VERTICAL_OFFSET, hints[0].upper(),
            hints[1].upper(), TEXT_VERTICAL_OFFSET, hints[2].upper(), IMAGE_TO_POST_FILE_PATH
    )

    addition = getTaskAuthorStr(user_data, author)
    text = "Задание №%d.\n%s" % (task_no, addition)

    return runShellCommand(cmd) and runSync() and postPictureAndTextOnTheWall(
            bot_data, text=text, attachments=[ LINK_TO_COMMUNITY_IM ], store_post_data=True)


# Store a task as task.png picture in the current directory.
# Returns True if succeeded, False otherwise
def storeTask(hints):
    if not hints:
        return
    # Produce an image to be posted on the community wall via shell command
    max_len = max([ len(item) for item in hints ])
    font = REGULAR_FONT if max_len < REGULAR_FONT_EDGE else CONDENSED_FONT
    input_filename = "background.jpg"
    output_filename = "task.png"

    if len(hints) == 1:
        cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d -fill white ' \
                '-annotate 0 "%s" %s' % (
                input_filename, font, FONT_POINT_SIZE, hints[0].upper(), output_filename
        )
    elif len(hints) == 2:
        cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d -fill white ' \
                '-annotate +0-%d "%s" -annotate +0+%d "%s" %s' % (
                input_filename, font, FONT_POINT_SIZE, TEXT_TWOROW_OFFSET, hints[0].upper(),
                TEXT_TWOROW_OFFSET, hints[1].upper(), output_filename
        )
    else:
        cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d -fill white ' \
                '-annotate +0-%d "%s" -annotate 0 "%s" -annotate +0+%d "%s" %s' % (
                input_filename, font, FONT_POINT_SIZE, TEXT_VERTICAL_OFFSET, hints[0].upper(),
                hints[1].upper(), TEXT_VERTICAL_OFFSET, hints[2].upper(), output_filename
        )

    return runShellCommand(cmd) and runSync()


# Posts correct answer to the previously posted task. Returns True if succeeded, False otherwise
def postAnswerOnTheWall(bot_data, user_data, task, task_no):
    # Gets previous task statistics and returns a dictionary of raw and
    # calculated values as well as calculated values string representation.
    def getTaskStat(user_data):
        attempts, successful, total = 0, 0, 0
        for k, u in user_data.items():
            if u[USER_ID] != REF_USER_ID:
                t = u[USER_TASKS][USER_TASKS_PREVIOUS]
                if t and t[USER_TASKS_ATTEMPTS] < ATTEMPTS_TASK_SOLVING:
                    total += 1
                    if t[USER_TASKS_COMPLETION_TIME]:
                        successful += 1
                        attempts += t[USER_TASKS_ATTEMPTS]
                pass

        lost_points = (successful * (ATTEMPTS_TASK_SOLVING - 1) - attempts) * WORTH_OF_ATTEMPT
        difficulty = ( 100 - (
            100 * (successful * WORTH_OF_TASK - lost_points) // (total * WORTH_OF_TASK)
            ) ) if total > 0 else -1
        attempts_avg = (ATTEMPTS_TASK_SOLVING - (float(attempts) / successful)) \
            if successful > 0 else -1.0
        s_difficulty = ("%d%%" % difficulty) if difficulty >= 0 else "-"
        s_attempts_avg = ("%5.3f" % attempts_avg) if attempts_avg >= 0 else "-"

        return dict(
            raw=dict(attempts=attempts, successful=successful, total=total),
            calc=dict(difficulty=difficulty, attempts_avg=attempts_avg),
            represented=dict(difficulty=s_difficulty, attempts_avg=s_attempts_avg)
        )

    # Produce an image to be posted on the community wall via shell command
    main_answer = task[BOT_TASKS_ANSWERS][0]
    answer_max_len = len(main_answer)
    additional_answer = ""
    if len(task[BOT_TASKS_ANSWERS]) == 2:
        additional_answer = task[BOT_TASKS_ANSWERS][1]
        answer_max_len = max(answer_max_len, len(additional_answer))

    font = REGULAR_FONT if answer_max_len < REGULAR_FONT_EDGE else CONDENSED_FONT

    if additional_answer:
        cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d -fill white ' \
                '-annotate +0-%d "%s" -annotate +0+%d "%s" %s' % (
                BACKGROUND_IMAGE, font, FONT_POINT_SIZE,
                TEXT_TWOROW_OFFSET, main_answer.upper(),
                TEXT_TWOROW_OFFSET, additional_answer.upper(),
                IMAGE_TO_POST_FILE_PATH
        )
    else:
        cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d -fill white ' \
                '-annotate 0 "%s" %s' % (
                BACKGROUND_IMAGE, font, FONT_POINT_SIZE,
                main_answer.upper(), IMAGE_TO_POST_FILE_PATH
        )

    if not runShellCommand(cmd) or not runSync():
        return False

    stat = getTaskStat(user_data)
    raw = stat["raw"]
    represented = stat["represented"]

    text = list()
    text.append( 'Ответ к заданию №%d' % task_no )
    delimiter = '-' * (2 * len(text[0]))
    text.append( delimiter )
    if len(task[BOT_TASKS_ANSWERS]) > 2:
        text.append( 'Также приняты в качестве ответов:' )
        for ans in task[BOT_TASKS_ANSWERS][1:]:
            text.append( ans.upper() )
        text.append( delimiter )
    text.append( 'Сложность: %s' % represented["difficulty"])

    res = postPictureAndTextOnTheWall(bot_data, text='\n'.join(text))
    if res:
        logMe(
            "Task %s stat: "
            "{ difficulty=%s, attempts_avg=%s }, "
            "{ attempts=%d, successful=%d, total=%d }\n"
            % (
                str(task[BOT_TASKS_HINTS]),
                represented["difficulty"], represented["attempts_avg"],
                raw["attempts"], raw["successful"], raw["total"]
            )
        )
    return res


# Returns string representation of the user id
def userIdToString(identifier, hide_id=False):
    if hide_id:
        return ('id%s%0' + str(ID_DIGITS_TO_SHOW) + 'd') % (
            ID_HIDDEN_CHAR * ID_HIDDEN_COUNT, identifier % 10**ID_DIGITS_TO_SHOW)
    else:
        return "id%d" % identifier


# Returns human readable representation of the user account
def toHumanReadable(identifier, first_name, last_name, hide_id=False):
    if first_name or last_name:
        hint = "%s %s" % (first_name, last_name)
        if len(hint) <= LIMIT_NAMES_TO:
            return hint
        if first_name:
            hint = "%s. %s" % (first_name[0], last_name)
        if len(hint) <= LIMIT_NAMES_TO:
            return hint
    return userIdToString(identifier, hide_id)


# Posts candidates to be a winner of the current series
# on the community wall and its wiki-pages.
# Returns True if succeeded, False otherwise
def postCandidates(bot_data, candidates, post_on_wall=False, text=None):

    # If there no candidates just do nothing
    if candidates:
        # Prepare wiki-content
        title = \
            "!<center>№</center>\n" \
            "!Участник\n" \
            "!<center>id страницы</center>\n" \
            "!<center>Баллы</center>\n"

        c = list()
        last_points, pos = -1, 0
        for i, cc in enumerate(candidates):
            if cc[1] == last_points:
                if i % RATINGS_ON_WIKI_PAGE == 0:
                    current = "|<center>%d</center>\n" % pos
                else:
                    current = "|\n"
            else:
                pos += 1
                current = "|<center>%d</center>\n" % pos
                last_points = cc[1]
            current += "|%s\n|<center>%s</center>\n|<center>%d</center>\n" % (
                toHumanReadable(cc[0], cc[3], cc[4], hide_id=True),
                userIdToString(cc[0], hide_id=True),
                cc[1]
            )
            c.append(current)
        header, footer = "{|noborder;nomargin\n|~35px 270px 100px 50px\n", "|}\n"
        new_count = (len(c) - 1) // RATINGS_ON_WIKI_PAGE + 1
        assert new_count > 0

    else:   # not candidates
        new_count = 1

    # Handle wiki-pages
    ok, pages = getWikiPages(r"РЕЙТИНГ( \(стр\. [0-9]+\))?")
    if not ok:
        return False

    old_count = len(pages)
    main_wiki_id = None

    # Fill in wiki pages with the current user ratings
    for i in range(new_count):
        paginator = getPaginatorFor(i, new_count)
        if paginator:
            paginator = "\n" + paginator
        since = i * RATINGS_ON_WIKI_PAGE
        toward = (i + 1) * RATINGS_ON_WIKI_PAGE
        if candidates:
            res = saveToWikiPage(
                    getRatingPageTitle(i),
                    "|-\n".join([header, title] + c[since:toward]) + footer + paginator
            )
        else:
            res = saveToWikiPage(
                getRatingPageTitle(i),
                "Здесь пока пусто, потому что бот Шурик ещё не получил"
                " вариантов ответов от участников.\n\nБудьте первым!\n"
            )
        if not res:
            ok = False

    # Clean up the pages with former ratings
    for p in pages[new_count:old_count]:
        if not saveToWikiPage(p, ""):
            ok = False

    if not ok:
        return False

    if not post_on_wall or not candidates:
        return True

    # Generate proper picture for a post on the wall
    cmd = 'convert %s -gravity center -font %s -weight heavy -pointsize %d ' \
            '-fill white -annotate 0 "%s" %s' % (
            RESULT_IMAGE, REGULAR_FONT, SMALL_FONT_POINT_SIZE,
            RATING_TEXT.upper(), IMAGE_TO_POST_FILE_PATH
    )

    if not runShellCommand(cmd) or not runSync():
        return False

    # Finally post the link on the community wall
    main_wiki_id = [ "page-%d_%d" % (COMMUNITY_ID, RATING_PAGE_ID) ]
    return postPictureAndTextOnTheWall(bot_data, text=text, attachments=main_wiki_id)


# Returns tuple of (success, pages) where 'success' is a boolean result of API
# request completion; 'pages' is a list of wiki pages matching against given
# template, each page is just its title.
def getWikiPages(template='.*'):
    if WIKI_CACHE:
        return True, sorted(WIKI_CACHE.keys())
    reqType = "pages.getTitles"
    data = dict(group_id=COMMUNITY_ID)
    reply = sendApiRequest(reqType, ADMIN_API_KEY, data)
    if reply and ('response' in reply):
        response = reply['response']
        if response:
            pattern = re.compile(template)
            pages = [ page['title']
                for page in response if pattern.fullmatch(page['title'])
            ]
            success = len(set(pages)) == len(pages)
            pages.sort()
            logMe("pages='%s'\n" % str(pages))
            for p in pages:
                if p not in WIKI_CACHE:
                    WIKI_CACHE[p] = None
            return success, pages

    handleReplyError(reply, reqType)
    return False, list()


# Returns a title for 'current' (zero based) wiki page containing user ratings
def getRatingPageTitle(current):
    return ("РЕЙТИНГ (стр. %d)" % (current + 1)) if current else "РЕЙТИНГ"


# Returns a paginator string containing links backward and forward of the wiki
# page set for 'current' (zero based) page of 'total' pages.
def getPaginatorFor(current, total):
    def getPageLink(pageNo, anchor):
        return "[[%s|%s]] " % (getRatingPageTitle(pageNo), anchor)

    if total <= 1:
        return ""

    first, last = 0, total - 1
    backward, forward = max(current - 1, first), min(current + 1, last)
    p = list()

    if first < current:
        p.append(getPageLink(first, "<<"))
        p.append(getPageLink(backward, "Пред."))

    for delta in range(-PAGINATOR_SIZE, PAGINATOR_SIZE+1):
        page = current + delta
        if delta == 0:
            p.append("<b>%d</b>" % (page + 1))
        elif page >= first and page <= last:
            p.append(getPageLink(page, "%d" % (page + 1)))

    if current < last:
        p.append(getPageLink(forward, "След."))
        p.append(getPageLink(last, ">>"))

    return "<center>%s</center>" % (" ".join(p))


# Saves the wiki page by its title with given text.
# Returns True if succeeded or False otherwise
def saveToWikiPage(title, text):
    if SIMULATE_TO:
        return True
    if title in WIKI_CACHE and text == WIKI_CACHE[title]:
        logMe('Wiki page "%s" is up to date' % title)
        return True
    reqType = "pages.save"
    data = dict(group_id=COMMUNITY_ID, title=title, text=text)
    reply = sendApiRequest(reqType, ADMIN_API_KEY, data)
    if reply and ('response' in reply):
        if reply['response']:
            logMe('Wiki page "%s" is updated' % title)
            WIKI_CACHE[title] = text
            return True
    handleReplyError(reply, reqType)
    return False


# Detects if the 'text' is a command for the bot and if it is executed by it.
# Returns (sent [[sent message id or False if fatal error occurred]], handled) tuple
def handleAdminCommand(bot_data, user_data, user, text):
    sent, handled = False, False
    if not user[USER_ADMIN] or SIMULATE_TO:
        return sent, handled

    text = text.strip()
    if not text.startswith(COMMAND_BEGINNING):
        return sent, handled

    fields = text[1:].lower().split(IN_COMMAND_SEPARATOR)
    for i, field in enumerate(fields):
        fields[i] = field.strip()

    # Since the command is from an admin and starting with COMMAND_BEGINNING char
    # consider it as an admin command and nothing else
    msg = ""
    handled = True

    for cmd_desc in ADMIN_COMMANDS:
        if cmd_desc['command'] == fields[0]:
            if len(fields) in cmd_desc['lengths']:
                logSR(bot_data)
                msg = cmd_desc['handler'](bot_data, user_data, fields, user)
                logMe("Admin command handler is passed")
                logSR(bot_data)
            else:
                msg = 'Ошибка формата команды "%s"' % fields[0]
            break
    else:
        msg = "Неизвестная команда"

    return sendMessage(user[USER_ID], msg, bot_data), handled


# Report start event
def logMe(s):
    print("%s: %s" % (dateTimeStr(), s), flush=True)


# Report start event
def logStartEvent():
    print("\n")
    logMe("Started\n\n")


# Report admin rights changed event
def logAdminRightsChangedEvent(str_user_id, status):
    logMe("Administrative rights changed for user (id=%s) to %s\n\n" % (str_user_id, str(status)))


# Report incoming text
def logIncomingText(user_id, text):
    logMe("<-- [%d]: '%s'\n" % (user_id, text))


# Report outgoing text
def logOutgoingText(user_id, text):
    logMe("--> [%d]: '%s'" % (user_id, text))


# Logs an error and returns False
def logError(s):
    logMe("Error - %s\n" % s);
    return False


# Logs an alert
def logAlert(bot_data, s):
    bot_data[BOT_ALERTS].append(s)
    logMe("ALERT '%s' has been added\n" % s);


def logSR(bot_data):
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr
    logMe("sr='%s'\n" % str(sr))


# Report an event to be handled, returns current series
def checkAndLogEvent(bot_data, event_name):
    sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
    assert sr

    if sr[BOT_SERIES_EXPECTED] == event_name and not bot_data[BOT_FLAG_PAUSED]:
        logMe("Event occurred: %s\n" % event_name)
        logSR(bot_data)
        return sr
    else:
        logMe("Event occurred: %s ... Skipped\n" % event_name)
        logSR(bot_data)
        return False


# Define admin commands
ADMIN_COMMANDS = [
    dict(
        command = "help",
        lengths = [ 1 ],
        handler = doHelp,
        desc = "выводит этот список"
    ),
    dict(
        command = "list",
        lengths = [ 1 ],
        handler = doList,
        desc = "выводит список заданий"
    ),
    dict(
        command = "add",
        lengths = [ 5, 6 ],
        handler = doAdd,
        desc = "добавляет новое задание в конец списка"
    ),
    dict(
        command = "insert",
        lengths = [ 6, 7 ],
        handler = doInsert,
        desc = "вставляет новое задание в указанное N место списка"
    ),
    dict(
        command = "delete",
        lengths = [ 2 ],
        handler = doDelete,
        desc = "удаляет задание по его номеру N"
    ),
    dict(
        command = "up",
        lengths = [ 2 ],
        handler = doUp,
        desc = "перемещает задание N вверх в списке"
    ),
    dict(
        command = "down",
        lengths = [ 2 ],
        handler = doDown,
        desc = "перемещает задание N вниз в списке"
    ),
    dict(
        command = "move",
        lengths = [ 3 ],
        handler = doMove,
        desc = "перемещает задание M в заданную позицию N в списке"
    ),
    dict(
        command = "start",
        lengths = [ 1 ],
        handler = doStart,
        desc = "запускает игру"
    ),
    dict(
        command = "pause",
        lengths = [ 1 ],
        handler = doPause,
        desc = 'приостанавливает работу бота до команды "/resume"'
    ),
    dict(
        command = "resume",
        lengths = [ 1 ],
        handler = doResume,
        desc = 'возобновляет работу бота, приостановленного командой "/pause"'
    ),
    dict(
        command = "reset",
        lengths = [ 1 ],
        handler = doReset,
        desc = "принудительно прекращает текущую серию игры и сбрасывает результаты"
    ),
    dict(
        command = "finalize",
        lengths = [ 1, 2 ],
        handler = doFinalize,
        desc = "принудительно останавливает текущую серию игры и " \
               "подводит её итоги, можно задать пороговый рейтинг претендентов"
    ),
    dict(
        command = "status",
        lengths = [ 1 ],
        handler = doStatus,
        desc = "показывает текущий статус игры"
    ),
    dict(
        command = "advance",
        lengths = [ 1 ],
        handler = doAdvance,
        desc = "продвигает игру"
    ),
    dict(
        command = "alert",
        lengths = [ 2 ],
        handler = doAlert,
        desc = "управляет подпиской отправителя на ALERTы"
    ),
]


# Define timestamps of the various events when they to be triggered firstly
now = datetime.utcnow()
ts = dateTimeSinceEpoch(now)
ANSWER_DEADLINE_TIME = timestampByDayTime(now, 10, 0, 15)
POST_TASK_TIME = timestampByDayTime(now, 10, 0, 45)
PUBLISH_ANSWER_TIME = timestampByDayTime(now, 13, 30, 0)
PUBLISH_CANDIDATES_TIME = timestampByDayTime(now, 19, 59, 0)
PUBLISH_WINNERS_TIME = timestampByDayTime(now, 20, 0, 0)


# Define upper limit for message id to be handled in simulation mode
MESSAGE_ID_UPPER_RANGE = 0


# Events to be handled
events = list()

# Refresh community members event
events.append( dict(
    handler = handleRefreshMembers,
    timestamp = ts,
    period = MEMBERS_POLLING_PERIOD,
    event_name = False
    ) )

events.append( dict(
    handler = handleRefreshNames,
    timestamp = ts + 1,
    period = NAMES_POLLING_PERIOD,
    event_name = False
    ) )

# Incoming messages polling event
events.append( dict(
    handler = handleIncomingMessages,
    timestamp = ts + 2,
    period = HANDLE_ALL_MESSAGES_PERIOD,
    event_name = False
    ) )

# Refresh community admins event
events.append( dict(
    handler = handleRefreshAdmins,
    timestamp = ts + ADMINS_POLLING_PERIOD,
    period = ADMINS_POLLING_PERIOD,
    event_name = False
    ) )

# Send alert to admins
events.append( dict(
    handler = handleSendAlert,
    timestamp = ts + 7,
    period = ALERT_SENDING_PERIOD,
    event_name = False
    ) )

# Answers deadline event
events.append( dict(
    handler = handleAnswersDeadline,
    timestamp = ts + ANSWER_DEADLINE_TIME,
    period = A_DAY_PERIOD,
    event_name = ANSWER_DEADLINE_TIME_EVENT
    ) )

# Post new task event
events.append( dict(
    handler = handlePostNewTask,
    timestamp = ts + POST_TASK_TIME,
    period = A_DAY_PERIOD,
    event_name = POST_TASK_TIME_EVENT
    ) )

# Publish answer event
events.append( dict(
    handler = handlePublishAnswer,
    timestamp = ts + PUBLISH_ANSWER_TIME,
    period = A_DAY_PERIOD,
    event_name = PUBLISH_ANSWER_TIME_EVENT
    ) )

# Publish candidates event
events.append( dict(
    handler = handlePublishCandidates,
    timestamp = ts + PUBLISH_CANDIDATES_TIME,
    period = A_DAY_PERIOD,
    event_name = PUBLISH_CANDIDATES_TIME_EVENT
    ) )

# Publish winners event
events.append( dict(
    handler = handlePublishWinners,
    timestamp = ts + PUBLISH_WINNERS_TIME,
    period = A_DAY_PERIOD,
    event_name = PUBLISH_WINNERS_TIME_EVENT
    ) )


# Parsed command line arguments
COMMUNITY_ID = False
COMMUNITY_API_KEY = False
ADMIN_API_KEY = False
BACKGROUND_IMAGE = False
RESULT_IMAGE = False
SIMULATE_TO = 0


# Data collections
user_data = False
bot_data = False



#
# Execution start point
#
def main():
    logStartEvent()

    global COMMUNITY_ID
    global COMMUNITY_API_KEY
    global ADMIN_API_KEY
    global BACKGROUND_IMAGE
    global RESULT_IMAGE
    global SIMULATE_TO

    global user_data
    global bot_data

    parser = argparse.ArgumentParser(description="VK bot driving a community")
    parser.add_argument(
            "-c", "--community", type=int, required=True, help="VK community identifier")
    parser.add_argument(
            "-b", "--bot-key", type=str, required=True, help="Bot's VK API key")
    parser.add_argument(
            "-a", "--admin-key", type=str, required=True, help="Admin's VK API key")
    parser.add_argument(
            "-i", "--background-image", type=str, required=True,
            help="Filename of task background image")
    parser.add_argument(
            "-t", "--result-image", type=str, required=True,
            help="Filename of result background image")
    parser.add_argument(
            "-s", "--simulate", type=int, help="Simulate up to the given message ID")
    args = parser.parse_args()

    COMMUNITY_ID = args.community
    COMMUNITY_API_KEY = args.bot_key
    ADMIN_API_KEY = args.admin_key
    BACKGROUND_IMAGE = args.background_image
    RESULT_IMAGE = args.result_image
    if args.simulate:
        SIMULATE_TO = args.simulate

    # Initialize the random numbers generator
    random.seed()

    # Read data files
    user_data = parseUserDataFile()
    bot_data = parseBotDataFile()

    if SIMULATE_TO:
        if SIMULATE_TO and bot_data[BOT_HANDLED_MESSAGE_ID] + 1 >= SIMULATE_TO:
            logMe("Nothing to simulate!")
            return

        if bot_data[BOT_FLAG_PAUSED]:
            logMe("Can not simulate when paused!")
            return
        sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
        assert sr
        if sr[BOT_SERIES_STATE] == SS_INACTIVE:
            logMe("Can not simulate when inactive!")
            return

        if not sr[BOT_SERIES_EXPECTED]:
            logMe("Can not simulate when expected event undefined!")
            return

        while sr[BOT_SERIES_STATE] in [ SS_OPERATIONAL, SS_PRIZED ] \
                and sr[BOT_SERIES_EXPECTED] \
                and sr[BOT_SERIES_EXPECTED] != PUBLISH_ANSWER_TIME_EVENT:
            res = doAdvance(bot_data, user_data, False, False)
            logMe("doAdvance() responses: %s" % res)

            sr = bot_data[BOT_SERIES][BOT_SERIES_CURRENT]
            assert sr
            if sr[BOT_SERIES_STATE] == SS_INACTIVE:
                logMe("Can not simulate when inactive!")
                return


    # Event handling loop
    while True:
        if SIMULATE_TO and bot_data[BOT_HANDLED_MESSAGE_ID] + 1 >= SIMULATE_TO:
            res = doAdvance(bot_data, user_data, False, False)
            logMe("doAdvance() responses: %s" % res)
            refreshBotDataFile(bot_data)
            refreshUserDataFile(user_data)
            logMe("Simulation completed")
            return

        ts = dateTimeSinceEpoch()
        future_ts = 0
        for event in events:
            if ts >= event['timestamp']:
                event['handler'](bot_data, user_data, event['event_name'])
                event['timestamp'] = event['period'] + ts
                refreshBotDataFile(bot_data)
                refreshUserDataFile(user_data)

            temp_ts = event['timestamp']
            if not future_ts or temp_ts < future_ts:
                future_ts = temp_ts

        ts = dateTimeSinceEpoch()
        if future_ts:
            time.sleep(max(future_ts - ts, 1))
        else:
            break


if __name__ == "__main__":
    # execute only if run as a script
    main()


# 1. Split to units
# 2. Try 'wand' to utilize imagemagick capabilities
# 3. Use LongPolling feature of VK API
